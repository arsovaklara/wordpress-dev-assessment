<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'influencers_club' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p+/Cd=W_#*!9Pg1#^U`X_RFI(|=~gl+5C&}w{`PbOh3V|?!y2>3)kIu@C$< Q^4i' );
define( 'SECURE_AUTH_KEY',  '^jmNeLa{NEzTl,<hwu1=w1{)Rt8MOn(YBrWTebf~9a?/Z!X G,NNeM-VfA7GK%`,' );
define( 'LOGGED_IN_KEY',    '06V1#@|jg!;XtOhHDQ:X:h.^5zV?fz6h4Q@ET1&ndKGj1&7i9!GV8W>/)2)@7V@q' );
define( 'NONCE_KEY',        '&^9*qu~V?aZ[|_A+C|7w+i4|[M+Zu{H~^u3az197-g8hGQguUpg8Po}9Nc9ky^J5' );
define( 'AUTH_SALT',        'N+%~n;SVUj l%3{jB[w<v!_?/MTv]MQ7-HY]Bd[Ry7e6vG1pbOl7zYqsm<S/6+-X' );
define( 'SECURE_AUTH_SALT', 'mQ&I+^VStj%9;E(pQDpq,evZ!BS?2Stt9&(Rf~ecz0&JZx@@9tM^]Cf716#i]*Ka' );
define( 'LOGGED_IN_SALT',   'PB>g(8nJw~4Li%h.+*IU|iN?oT;7MO7apeq8W!L$-Fm;5OSm!TKmvkWI=0eGUdpd' );
define( 'NONCE_SALT',       '[AlcAEbhBpI6ThH+s?4BacHfSxe#`Arl_0a$ZJO8}Wl-i)l}f-PO!f3kV-UR!q|a' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
