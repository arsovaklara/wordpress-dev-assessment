<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title of the page -->
    <title>Unsplash</title>
    <!-- Link to the Montserrat font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,700" rel="stylesheet">
    <!-- Styles -->
    <style>
        body {
            font-size: 16px;
            color: #404040;
            font-family: 'Montserrat', sans-serif !important;
            background-color: #fff;
            margin: 0;
            padding: 2rem;
            display: grid;
            place-items: center;
            box-sizing: border-box;
        }

        #scraped-data-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            margin: 0 15em;
        }

        .user {
            background-color: white;
            max-width: 390px;
            display: flex;
            flex-direction: column;
            overflow: hidden;
            border-style: solid;
            border-color: #0000001A;
            border-width: 1px 1px 1px 1px;
            border-radius: 10px;
            box-shadow: 4px 4px 12px 0px rgba(0, 0, 0, 0.1);
            width: 45%;
            margin-bottom: 20px;
        }

        .user_img {
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            height: 11rem;
            display: flex;
            align-items: flex-end;
            justify-content: center;
            box-sizing: border-box;
        }

        h2.name {
            text-align: center;
            font-size: 48px;
            padding: 0.5rem 0 0;
            margin: 10px;
            height: 120px;
        }

        .username {
            color: lighten(#404040, 50%);
            font-size: 0.85rem;
            text-align: center;
            padding: 1.2rem 0;
        }

        .actions {
            padding: 1.2rem 0;
            display: flex;
            flex-direction: column;
            order: 99;
        }

        .total {
            padding-bottom: 1rem;
            display: flex;
        }

        .total h2 {
            text-align: center;
            width: 50%;
            margin: 0;
            box-sizing: border-box;
        }

        .total a {
            text-decoration: none;
            padding: 0.8rem;
            display: flex;
            flex-direction: column;
            border-radius: 0.8rem;
            transition: background-color 100ms ease-in-out;
        }

        .total span {
            color: #3484dd;
            font-weight: bold;
            transform-origin: bottom;
            transform: scaleY(1.3);
        }

        .total small {
            color: #afafaf;
            font-size: 0.85rem;
            font-weight: normal;
        }

        .insta_btn {
            text-align: center;
        }

        .insta_btn button {
            color: #0E2E4D;
            font-size: 14px;
            font-weight: bold;
            background-color: #F0C933;
            width: 50%;
            border: none;
            padding: 15px;
            margin-top: 3em;
            outline: none;
            box-sizing: border-box;
            border-radius: 1.5rem / 50%;
        }

        .insta_btn button a {
            text-decoration: none;
        }

        .insta_btn button:hover {
            background-color: #efb10a;
        }

        .insta_btn button:active {
            text-decoration: none;
            background-color: #e8a200;
            transform: scale(1);
        }

        .bio {
            text-align: center;
            padding: 2.5rem 2rem;
            order: 100;
        }

        @media (min-width: 260px) and (max-width: 479px) {

            /* Media query for small screens */
            #scraped-data-container {
                display: grid;
                margin: 0 1rem;
            }

            h2.name {
                font-size: 34px;
                height: 100px;
            }

            .total span {
                font-size: 34px;
            }

            .insta_btn button {
                width: 64%;
                font-size: 14px;
            }

            .user {
                width: auto;
            }

            .bio {
                font-size: 14px;
            }
        }

        @media (min-width: 480px) and (max-width: 600px) {

            #scraped-data-container {
                margin: 0 1rem;
                display: block;
            }

            .user {
                width: 100%;
            }

            .insta_btn button {
                width: 80%;
            }
        }

        @media (min-width: 601px) and (max-width: 768px) {

            #scraped-data-container {
                margin: 0 1rem;
            }

            .total span {
                font-size: 36px;
            }

            .insta_btn button {
                width: 60%;
            }

            .bio {
                font-size: 16px;
            }

            h2.name {
                font-size: 44px;
            }
        }

        @media (min-width: 769px) and (max-width: 1024px) {

            #scraped-data-container {
                margin: 0 4rem;
            }
        }

        @media (min-width: 1025px) and (max-width: 1200px) {

            #scraped-data-container {
                margin: 0 10rem;
            }
        }

        @media (min-width: 1201px) {

            #scraped-data-container {
                margin: 0 12rem;
            }
        }
    </style>
</head>

<body>
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            the_content();
        }
    }

    get_header(); // Include WordPress header
    ?>

    <!-- Container for scraped data -->
    <div id="scraped-data-container"></div>

    <?php get_footer(); // Include WordPress footer  ?>

    <!-- Enqueue jQuery (if not already enqueued) -->
    <?php if (!wp_script_is('jquery', 'enqueued')): ?>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <?php endif; ?>

    <script>
        jQuery(document).ready(function ($) {
            // Unsplash API access key
            var unsplashApiKey = 'Sp8KfK9x-3YJvqEik7dvGKi5iZTDcyNQ8AeB3MIwPoQ';
            // Unsplash API endpoint
            var unsplashApiEndpoint = 'https://api.unsplash.com/photos/random?count=15&client_id=' + unsplashApiKey;

            // Fetch data from Unsplash API using AJAX
            $.ajax({
                url: unsplashApiEndpoint,
                method: 'GET',
                dataType: 'json',
                success: function (data) {
                    // Call the function to display Unsplash data
                    displayUnsplashData(data);
                },
                error: function (error) {
                    // Log an error message if fetching fails
                    console.error('Error fetching Unsplash data:', error);
                }
            });

            // Function to display Unsplash data
            function displayUnsplashData(data) {
                // Get the container where data will be displayed
                var container = $('#scraped-data-container');

                // Loop through each user's data
                data.forEach(function (photographer) {
                    if (!photographer.user.bio) {
                        return; // Skip if bio is empty
                    }

                    // Create a div for each user
                    var userDiv = $('<div>').addClass('user');

                    // Add user image
                    var userImgDiv = $('<div>').addClass('user_img');
                    userImgDiv.css('background-image', 'url(' + photographer.user.profile_image.large + ')');
                    userDiv.append(userImgDiv);

                    // Add username
                    var usernameDiv = $('<div>').addClass('username').html('<strong>Username: </strong>' + photographer.user.username);
                    userDiv.append(usernameDiv);
                    
                    // Add user's name
                    var nameH2 = $('<h2>').addClass('name').text(photographer.user.name);
                    userDiv.append(nameH2);

                    var actionsDiv = $('<div>').addClass('actions');

                    // div for total likes and photos
                    var totalDiv = $('<div>').addClass('total');

                    var likesH2 = $('<h2>').html('<a href="#"><span>' + photographer.user.total_likes + '</span><small>Total Likes</small></a>');
                    totalDiv.append(likesH2);

                    var photosH2 = $('<h2>').html('<a href="#"><span>' + photographer.user.total_photos + '</span><small>Total Photos</small></a>');
                    totalDiv.append(photosH2);

                    actionsDiv.append(totalDiv);

                    // Add Instagram button
                    var instaBtnDiv = $('<div>').addClass('insta_btn');

                    var instaButton = $('<button>').html('<a href="https://www.instagram.com/' + photographer.user.instagram_username + '" target="_blank">Instagram Profile</a>');
                    instaBtnDiv.append(instaButton);

                    actionsDiv.append(instaBtnDiv);

                    userDiv.append(actionsDiv);

                    // Add bio paragraph
                    var bioParagraph = $('<p>').addClass('bio').html('<strong>Bio: </strong>' + photographer.user.bio);
                    userDiv.append(bioParagraph);

                    // Append user div to the container
                    container.append(userDiv);
                });
            }
        });
    </script>
</body>

</html>